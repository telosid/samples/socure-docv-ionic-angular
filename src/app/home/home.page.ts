import { Component } from '@angular/core';
import {
  ICaptureSelfieResult,
  IGetScannedLicenseResult,
  IGetScannedPassportResult,
  IScanLicenseResult,
  IScanPassportResult,
  IUploadScannedInfoResult,
  SocureDocV as Socure
} from '@telosid/capacitor-plugin-socure-docv';
import { environment } from '../../environments/environment';

const {
  socurePublicKey = ``
} = environment;

const IMAGE_URI_PREFIX = `data:image/png;base64,`;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  idDoc: string;
  captureSelfieResult: ICaptureSelfieResult;
  licenseData: IScanLicenseResult;
  passportData: IScanPassportResult;
  scannedLicenseResult: IGetScannedLicenseResult;
  scannedPassportResult: IGetScannedPassportResult;
  imageUriPrefix = IMAGE_URI_PREFIX;
  licenseDataString: string;
  passportDataString: string;
  uploadScannedInfoResult: IUploadScannedInfoResult;
  uploadScannedInfoResultString: string;

  constructor() {
  }

  async scanLicense() {
    try {
      const licenseData: IScanLicenseResult = await Socure.scanLicense();
      console.log('scanLicense() response', licenseData);
      this.licenseData = licenseData;
      this.licenseDataString = JSON.stringify(licenseData);
    } catch (error) {
      console.error(`Socure.scanLicense() Error`, error);
    }
    try {
      const scannedLicense: IGetScannedLicenseResult = await Socure.getScannedLicense();
      console.log('Socure.getScannedLicense() response', scannedLicense);
      this.scannedLicenseResult = scannedLicense;
    } catch (error) {
      console.error(`Socure.getScannedLicense() Error`, error);
    }
  }

  async scanPassport() {
    try {
      const passportInfo: IScanPassportResult = await Socure.scanPassport();
      console.log('Socure.scanPassport() response', passportInfo);
      this.passportData = passportInfo;
      this.passportDataString = JSON.stringify(passportInfo);
    } catch (error) {
      console.log('Socure.scanPassport() Error', error);
    }
    try {
      const scannedPassport: IGetScannedPassportResult = await Socure.getScannedPassport();
      console.log('Socure.getScannedPassport() response', scannedPassport);
      this.scannedPassportResult = scannedPassport;
    } catch (error) {
      console.error(`Socure.getScannedPassport() Error`, error);
    }
  }

  async captureSelfie() {
    try {
      const captureSelfieResult: ICaptureSelfieResult = await Socure.captureSelfie();
      console.log('Socure.captureSelfie() response', captureSelfieResult);
      this.captureSelfieResult = captureSelfieResult;
    } catch (error) {
      console.error(`Socure.captureSelfie() Error`, error);
    }
  }

  async uploadScannedInfo() {
    try {
      const uploadResponse: IUploadScannedInfoResult = await Socure.uploadScannedInfo({
        socurePublicKey
      });
      console.log(`Socure.uploadScannedInfo() response`, uploadResponse);
      this.uploadScannedInfoResult = uploadResponse;
      this.uploadScannedInfoResultString = JSON.stringify(uploadResponse);
    } catch (error) {
      console.error(`Socure.uploadScannedInfo() Error`, error);
    }
  }

  clearSelfie() {
    this.captureSelfieResult = null;
  }

  clearPassport() {
    this.scannedPassportResult = null;
    this.passportData = null;
    this.passportDataString = ``;
  }

  clearLicense() {
    this.scannedLicenseResult = null;
    this.licenseData = null;
    this.licenseDataString = ``;
  }
}
