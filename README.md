# Getting Started

This example app will be using `Ionic Framework` and  `capacitor`.

- Please follow the following **Getting Started** guides
  - https://capacitorjs.com/docs
  - https://ionicframework.com/docs

## Ionic Angular

```bash
ionic start socure-docv-ionic-angular blank --capacitor --type=angular --package-id=com.socure.docv.ionic.example.angular
cd socure-docv-ionic-angular
```

## Native project setup

Build the web app

```bash
npm run build
```

Add Android and iOS project folders

```bash
npm install @capacitor/android @capacitor/ios
npx cap add android
npx cap add ios
```

### Verify native projects run pre-installation

* You should be able to run the blank application on a hardware device for both the Android and iOS projects at this
  point.

### **Important:**

* Make sure you can build and run both platforms before trying to add the Socure DocV capacitor plugin.

Add the following `scripts` to your `package.json`

```
"start:android": "ionic capacitor run android -l --external",
"start:ios": "ionic capacitor run ios -l --external",
"open:android": "ionic capacitor open android",
"open:ios": "ionic capacitor open ios"
```

#### Verify iOS

* Open the iOS project in Xcode and make sure the app signing is configured

```
npm run open:ios
```

* Go to the Signing & Capabilities tab and select a Team
* Plug in a hardware device

```
npm run start:ios
```

* Select your hardware device from the list presented

#### Verify Android

```
npm run start:android
```

* Select your Android device from the list presented

## Install the Socure DocV Capacitor plugin

* Follow the setup guide for iOS and Android in the plugin's [README](https://gitlab.com/telosid/plugins/capacitor-plugin-socure-docv)

### Verify native project run post-installation

#### Verify iOS

```
npm run start:ios
```

* Select your hardware device from the list presented

#### Verify Android

```
npm run start:android
```

* Select your Android device from the list presented

## Add User Interface to test plugin functionality

* Replace the `Blank` home page
  * Replace the contents of `src/app/home/home.page.html` with the following
```html
  <ion-header [translucent]="true">
  <ion-toolbar>
    <ion-title>
      Socure DocV Ionic Capacitor
    </ion-title>
  </ion-toolbar>
  </ion-header>
  
  <ion-content [fullscreen]="true">
  <ion-header collapse="condense">
  <ion-toolbar>
  <ion-title size="large">Socure DocV Ionic Capacitor</ion-title>
  </ion-toolbar>
  </ion-header>
  
    <ion-card>
      <ion-card-content>
        <div *ngIf="captureSelfieResult">
          <div
            *ngIf="captureSelfieResult.image"
          >
            <ion-img
              class="card-image"
              [src]="imageUriPrefix + captureSelfieResult.image"
            ></ion-img>
          </div>
        </div>
        <div
          *ngIf="!captureSelfieResult"
          style="width: 100px; margin: auto;"
        >
          <ion-img src="/assets/images/selfie.svg"></ion-img>
        </div>
        <ion-button
          *ngIf="!captureSelfieResult"
          class="ion-margin-top"
          size="large"
          expand="block"
          (click)="captureSelfie()"
        >
          Take Selfie
        </ion-button>
        <ion-button
          *ngIf="captureSelfieResult"
          class="ion-margin-top"
          size="large"
          expand="block"
          color="danger"
          (click)="clearSelfie()"
        >
          Clear
        </ion-button>
      </ion-card-content>
    </ion-card>
  
    <ion-card>
      <ion-item>
        <ion-label>ID Document</ion-label>
        <ion-select
          placeholder="Select One"
          [(ngModel)]="idDoc"
        >
          <ion-select-option value="PP">Passport</ion-select-option>
          <ion-select-option value="DL">Driver's License</ion-select-option>
        </ion-select>
      </ion-item>
      <ion-card-content *ngIf="idDoc">
        <div *ngIf="idDoc === 'PP'">
          <div *ngIf="scannedPassportResult">
            <div
              *ngIf="scannedPassportResult.frontImage"
              style="max-width: 500px; margin: auto;"
            >
              <ion-img
                class="card-image"
                [src]="imageUriPrefix + scannedPassportResult.frontImage"
              ></ion-img>
            </div>
          </div>
          <div
            *ngIf="!scannedPassportResult"
            style="width: 100px; margin: auto;"
          >
            <ion-img src="/assets/images/passport.svg"></ion-img>
          </div>
          <ion-button
            *ngIf="!scannedPassportResult"
            class="ion-margin-top"
            size="large"
            expand="block"
            (click)="scanPassport()"
          >
            Scan Passport
          </ion-button>
          <ion-button
            *ngIf="scannedPassportResult"
            class="ion-margin-top"
            size="large"
            expand="block"
            color="danger"
            (click)="clearPassport()"
          >
            Clear
          </ion-button>
        </div>
        <div *ngIf="idDoc === 'DL'">
          <div *ngIf="scannedLicenseResult">
            <div
              *ngIf="scannedLicenseResult.frontImage"
              style="max-width: 500px; margin: auto;"
            >
              <ion-img
                class="card-image"
                [src]="imageUriPrefix + scannedLicenseResult.frontImage"
              ></ion-img>
            </div>
            <div
              *ngIf="scannedLicenseResult.backImage"
              style="max-width: 500px; margin: auto;"
            >
              <ion-img
                class="card-image"
                [src]="imageUriPrefix + scannedLicenseResult.backImage"
              ></ion-img>
            </div>
          </div>
          <div
            *ngIf="!scannedLicenseResult"
            style="width: 100px; margin: auto;"
          >
            <ion-img src="/assets/images/driver-license-car.svg"></ion-img>
          </div>
          <ion-button
            *ngIf="!scannedLicenseResult"
            class="ion-margin-top"
            size="large"
            expand="block"
            (click)="scanLicense()"
          >
            Scan ID
          </ion-button>
          <ion-button
            *ngIf="scannedLicenseResult"
            class="ion-margin-top"
            size="large"
            expand="block"
            color="danger"
            (click)="clearLicense()"
          >
            Clear
          </ion-button>
        </div>
      </ion-card-content>
    </ion-card>
    <ion-card-content>
      <ion-button
        size="large"
        expand="block"
        [disabled]="!(captureSelfieResult && (scannedLicenseResult || scannedPassportResult))"
        (click)="uploadScannedInfo()"
      >
        Upload Scanned Info
      </ion-button>
    </ion-card-content>
  </ion-content>
```
  * Replace the contents of `src/app/home/home.page.ts` with the following
```typescript
import { Component } from '@angular/core';
import {
  ICaptureSelfieResult,
  IGetScannedLicenseResult, IGetScannedPassportResult,
  IScanLicenseResult, IScanPassportResult, IUploadScannedInfoResult,
  SocureDocV as Socure
} from '@telosid/capacitor-plugin-socure-docv';
import { environment } from '../../environments/environment';

const {
  socurePublicKey = ``
} = environment;

const IMAGE_URI_PREFIX = `data:image/png;base64,`;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  idDoc: string;
  captureSelfieResult: ICaptureSelfieResult;
  licenseData: IScanLicenseResult;
  passportData: IScanPassportResult;
  scannedLicenseResult: IGetScannedLicenseResult;
  scannedPassportResult: IGetScannedPassportResult;
  imageUriPrefix = IMAGE_URI_PREFIX;

  constructor() {
  }

  async scanLicense() {
    try {
      const licenseData: IScanLicenseResult = await Socure.scanLicense();
      console.log('scanLicense() response', licenseData);
      this.licenseData = licenseData;
    } catch (error) {
      console.error(`Socure.scanLicense() Error`, error);
    }
    try {
      const scannedLicense: IGetScannedLicenseResult = await Socure.getScannedLicense();
      console.log('Socure.getScannedLicense() response', scannedLicense);
      this.scannedLicenseResult = scannedLicense;
    } catch (error) {
      console.error(`Socure.getScannedLicense() Error`, error);
    }
  }

  async scanPassport() {
    try {
      const passportInfo: IScanPassportResult = await Socure.scanPassport();
      console.log('Socure.scanPassport() response', passportInfo);
      this.passportData = passportInfo;
    } catch (error) {
      console.log('Socure.scanPassport() Error', error);
    }
    try {
      const scannedPassport: IGetScannedPassportResult = await Socure.getScannedPassport();
      console.log('Socure.getScannedPassport() response', scannedPassport);
      this.scannedPassportResult = scannedPassport;
    } catch (error) {
      console.error(`Socure.getScannedPassport() Error`, error);
    }
  }

  async captureSelfie() {
    try {
      const captureSelfieResult: ICaptureSelfieResult = await Socure.captureSelfie();
      console.log('Socure.captureSelfie() response', captureSelfieResult);
      this.captureSelfieResult = captureSelfieResult;
    } catch (error) {
      console.error(`Socure.captureSelfie() Error`, error);
    }
  }

  async uploadScannedInfo() {
    try {
      const uploadResponse: IUploadScannedInfoResult = await Socure.uploadScannedInfo({
        socurePublicKey
      });
      console.log(`Socure.uploadScannedInfo() response`, uploadResponse);
    } catch (error) {
      console.error(`Socure.uploadScannedInfo() Error`, error);
    }
  }

  clearSelfie() {
    this.captureSelfieResult = null;
  }

  clearPassport() {
    this.scannedPassportResult = null;
  }

  clearLicense() {
    this.scannedLicenseResult = null;
  }
}
  
```

* Run the application on both native devices
```
npm run start:ios
npm run start:android
```
